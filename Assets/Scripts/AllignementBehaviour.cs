﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Flock/Behaviour/Allignement")]
public class AllignementBehaviour : FlockBehaviour {

    public override Vector2 CalculateMove(FlockAgent agent, List<Transform> context, Flock flock)
    {
        if (context.Count == 0)
            return agent.transform.up;

        Vector2 allignementMove = Vector2.zero;
        foreach (Transform item in context)
        {
            allignementMove += (Vector2)item.transform.up;
        }
        allignementMove /= context.Count;
        return allignementMove;
    }
}
