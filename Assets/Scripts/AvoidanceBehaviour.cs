﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Flock/Behaviour/Avoidance")]
public class AvoidanceBehaviour : FlockBehaviour {

    public override Vector2 CalculateMove(FlockAgent agent, List<Transform> context, Flock flock)
    {
        if (context.Count == 0)
            return Vector2.zero;

        Vector2 avoidanceMove = Vector2.zero;
        int cantAvoid = 0;
        foreach (Transform item in context)
        {
            //Debug.Log(item.name + " " + Vector2.SqrMagnitude(item.position - agent.transform.position) + " < " + flock.GetSquareAvoidanceRadius());
            if (Vector2.SqrMagnitude(item.position - agent.transform.position) < flock.GetSquareAvoidanceRadius())
            {
                cantAvoid++;   
                avoidanceMove += (Vector2)(agent.transform.position - item.position);
            }
        }
        if (cantAvoid > 0)
            avoidanceMove /= cantAvoid;
        return avoidanceMove;
    }
}
