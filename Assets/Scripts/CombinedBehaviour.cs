﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Flock/Behaviour/Combined")]
public class CombinedBehaviour : FlockBehaviour
{
    public FlockBehaviour[] behaviours;
    public float[] weights;

    public override Vector2 CalculateMove(FlockAgent agent, List<Transform> context, Flock flock)
    {
        if(weights.Length != behaviours.Length)
        {
            Debug.LogError("Data missmatch in " + name, this);
            return Vector2.zero;
        }

        Vector2 move = Vector2.zero;

        for (int i = 0; i < behaviours.Length; i++)
        {
            Vector2 partialmove = behaviours[i].CalculateMove(agent, context, flock) * weights[i];

            if(partialmove != Vector2.zero)
            {
                if(partialmove.sqrMagnitude > weights[i] * weights[i])
                {
                    partialmove.Normalize();
                    partialmove *= weights[i];
                }
                move += partialmove;
            }
        }
        return move;
    }
}
