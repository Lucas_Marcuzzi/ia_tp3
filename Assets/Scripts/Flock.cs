﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flock : MonoBehaviour {

    public FlockAgent agentPrefab;
    List<FlockAgent> agents = new List<FlockAgent>();
    public FlockBehaviour behaviour;

    public int ammount = 250;
    const float agentDensity = 0.08f;

    public float driveFactor = 10.0f;
    public float maxSpeed = 5.0f;
    public float neightbourRadius = 1.5f;
    public float avoidanceRadiusMultiplier = 0.5f;

    float squareMaxSpeed;
    float squareNeighbourRadius;
    float squareAvoidanceRadius;


	// Use this for initialization
	void Start () {
        squareMaxSpeed = maxSpeed * maxSpeed;
        squareNeighbourRadius = neightbourRadius + neightbourRadius;
        squareAvoidanceRadius = squareNeighbourRadius * avoidanceRadiusMultiplier * avoidanceRadiusMultiplier;

        for (int i = 0; i < ammount; i++)
        {
            FlockAgent newAgent = Instantiate(agentPrefab, Random.insideUnitCircle * ammount * agentDensity,Quaternion.Euler(Vector3.forward * Random.Range(0,360f)),transform);
            newAgent.name = "Ship Nr." + i;
            agents.Add(newAgent);
        }

	}
	
	// Update is called once per frame
	void Update () {
		
        for (int i = 0; i < agents.Count; i++)
        {
            List<Transform> Context = GetNearbyObjects(agents[i]);

            //agents[i].GetComponentInChildren<SpriteRenderer>().color = Color.Lerp(Color.white, Color.red, Context.Count / 6f);

            Vector2 move = behaviour.CalculateMove(agents[i], Context, this);
            move *= driveFactor;
            if (move.sqrMagnitude > squareMaxSpeed)
                move = move.normalized * maxSpeed;
            agents[i].Move(move);
        }

	}
    
    public float GetSquareAvoidanceRadius()
    {
        return squareAvoidanceRadius;
    }

    List<Transform> GetNearbyObjects(FlockAgent agent)
    {
        List<Transform> context = new List<Transform>();
        Collider2D[] contextColliders = Physics2D.OverlapCircleAll(agent.transform.position, neightbourRadius);
        for (int i = 0; i<contextColliders.Length; i++)
        {
            if (contextColliders[i] != agent.GetCollider())
            {
                context.Add(contextColliders[i].transform);
                
            }
        }
        return context;
    }

}
