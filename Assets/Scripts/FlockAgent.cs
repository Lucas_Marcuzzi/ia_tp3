﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class FlockAgent : MonoBehaviour {

    Collider2D objectCollider;

	// Use this for initialization
	void Start () {
        objectCollider = GetComponent<Collider2D>();
	}
	
	public void Move(Vector2 velocity)
    {
        transform.up = velocity;
        transform.position += (Vector3)velocity * Time.deltaTime;
    }

    public Collider2D GetCollider()
    {
        return objectCollider;
    }
}
